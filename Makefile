# Makefile for the imgsizer distribution

# Change this in NEWS file, too
VERS=$(shell sed <imgsizer -n -e '/version *= *\"\(.*\)\"/s//\1/p')

# These choices are correct for Linux.  Season to taste.
BINDIR = /usr/bin
MANDIR = /usr/share/man/man1

install: imgsizer.1 uninstall
	cp imgsizer $(BINDIR)
	cp imgsizer.1 $(MANDIR)/imgsizer.1

uninstall:
	rm -f $(BINDIR)/imgsizer $(BINDIR)/imgsizer $(MANDIR)/imgsizer.1

clean:
	rm -f imgsizer.1 *~ imgsizer-*.tar.gz
	rm -f test-after.html index.html imgsizer.html

imgsizer.1: imgsizer.xml
	xmlto man imgsizer.xml

imgsizer.html: imgsizer.xml
	xmlto html-nochunks imgsizer.xml

# The original unshrunk crate logo is at
# http://clipart-library.com/image_gallery/22229.jpg
SOURCES = README COPYING NEWS control Makefile imgsizer imgsizer.xml \
	test-before.html test-converted.html test.png tnhd2.jpg crate.png

imgsizer-$(VERS).tar.gz: $(SOURCES) imgsizer.1 
	mkdir imgsizer-$(VERS)
	cp $(SOURCES) imgsizer.1 imgsizer-$(VERS)
	tar -czf imgsizer-$(VERS).tar.gz imgsizer-$(VERS)
	rm -fr imgsizer-$(VERS)
	ls -l imgsizer-$(VERS).tar.gz

pylint:
	@pylint --score=n imgsizer

NEWSVERSION=$(shell sed -n <NEWS '/^[0-9]/s/:.*//p' | head -1)

version:
	@echo "Internal version:" $(VERS) "NEWS file version:" $(NEWSVERSION)

# Regression test is good if there is no output
check: 
	@imgsizer <test-before.html >test-after.html
	@diff -c test-after.html test-converted.html
	@echo "No output (other than this) is good news"

dist: imgsizer-$(VERS).tar.gz

release: imgsizer-$(VERS).tar.gz imgsizer.html
	@[ $(VERS) = $(NEWSVERSION) ] || { echo "Version mismatch!"; exit 1; }
	shipper version=$(VERS) | sh -e -x

refresh: imgsizer.html
	@[ $(VERS) = $(NEWSVERSION) ] || { echo "Version mismatch!"; exit 1; }
	shipper -N -w version=$(VERS) | sh -e -x
